using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[DiagramInfoAttribute("CityDemo")]
public class BuildingViewModelBase : ViewModel {
    
    public P<Vector2> _PositionProperty;
    
    public BuildingViewModelBase(BuildingControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public BuildingViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
        _PositionProperty = new P<Vector2>(this, "Position");
    }
}

public partial class BuildingViewModel : BuildingViewModelBase {
    
    private FieldManagerViewModel _ParentFieldManager;
    
    public BuildingViewModel(BuildingControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public BuildingViewModel() : 
            base() {
    }
    
    public virtual P<Vector2> PositionProperty {
        get {
            return this._PositionProperty;
        }
    }
    
    public virtual Vector2 Position {
        get {
            return _PositionProperty.Value;
        }
        set {
            _PositionProperty.Value = value;
        }
    }
    
    public virtual FieldManagerViewModel ParentFieldManager {
        get {
            return this._ParentFieldManager;
        }
        set {
            _ParentFieldManager = value;
        }
    }
    
    protected override void WireCommands(Controller controller) {
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
        stream.SerializeVector2("Position", this.Position);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
        		this.Position = stream.DeserializeVector2("Position");;
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
        list.Add(new ViewModelPropertyInfo(_PositionProperty, false, false, false));
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class FieldManagerViewModelBase : ViewModel {
    
    public P<CommonBuildingViewModel> _CurrentCommonBuildingProperty;
    
    public P<CommonBuildingViewModel> _OldCommonBuildingProperty;
    
    public P<UniqueBuildingViewModel> _CurrentUniqueBuildingProperty;
    
    public P<UniqueBuildingViewModel> _OldUniqueBuildingProperty;
    
    public P<TileViewModel> _CurrentSelectedTileProperty;
    
    public P<TileViewModel> _OldSelectedTileProperty;
    
    public P<Int32> _TileCountLineProperty;
    
    public P<Int32> _TileCountColumnProperty;
    
    public ModelCollection<CommonBuildingViewModel> _CommonBuildingButtonsProperty;
    
    public ModelCollection<BuildingViewModel> _CommonBuildingsProperty;
    
    public ModelCollection<UniqueBuildingViewModel> _UniqueBuildingButtonsProperty;
    
    public ModelCollection<BuildingViewModel> _UniqueBuildingsProperty;
    
    public ModelCollection<TileViewModel> _TilesProperty;
    
    protected CommandWithSender<FieldManagerViewModel> _SetupField;
    
    protected CommandWithSenderAndArgument<FieldManagerViewModel, CommonBuildingViewModel> _InstantiateCommonBuilding;
    
    protected CommandWithSenderAndArgument<FieldManagerViewModel, UniqueBuildingViewModel> _InstantiateUniqueBuilding;
    
    protected CommandWithSender<FieldManagerViewModel> _Save;
    
    protected CommandWithSender<FieldManagerViewModel> _Load;
    
    public FieldManagerViewModelBase(FieldManagerControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public FieldManagerViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
        _CurrentCommonBuildingProperty = new P<CommonBuildingViewModel>(this, "CurrentCommonBuilding");
        _OldCommonBuildingProperty = new P<CommonBuildingViewModel>(this, "OldCommonBuilding");
        _CurrentUniqueBuildingProperty = new P<UniqueBuildingViewModel>(this, "CurrentUniqueBuilding");
        _OldUniqueBuildingProperty = new P<UniqueBuildingViewModel>(this, "OldUniqueBuilding");
        _CurrentSelectedTileProperty = new P<TileViewModel>(this, "CurrentSelectedTile");
        _OldSelectedTileProperty = new P<TileViewModel>(this, "OldSelectedTile");
        _TileCountLineProperty = new P<Int32>(this, "TileCountLine");
        _TileCountColumnProperty = new P<Int32>(this, "TileCountColumn");
        _CommonBuildingButtonsProperty = new ModelCollection<CommonBuildingViewModel>(this, "CommonBuildingButtons");
        _CommonBuildingButtonsProperty.CollectionChanged += CommonBuildingButtonsCollectionChanged;
        _CommonBuildingsProperty = new ModelCollection<BuildingViewModel>(this, "CommonBuildings");
        _CommonBuildingsProperty.CollectionChanged += CommonBuildingsCollectionChanged;
        _UniqueBuildingButtonsProperty = new ModelCollection<UniqueBuildingViewModel>(this, "UniqueBuildingButtons");
        _UniqueBuildingButtonsProperty.CollectionChanged += UniqueBuildingButtonsCollectionChanged;
        _UniqueBuildingsProperty = new ModelCollection<BuildingViewModel>(this, "UniqueBuildings");
        _UniqueBuildingsProperty.CollectionChanged += UniqueBuildingsCollectionChanged;
        _TilesProperty = new ModelCollection<TileViewModel>(this, "Tiles");
        _TilesProperty.CollectionChanged += TilesCollectionChanged;
    }
    
    protected virtual void CommonBuildingButtonsCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs args) {
    }
    
    protected virtual void CommonBuildingsCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs args) {
    }
    
    protected virtual void UniqueBuildingButtonsCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs args) {
    }
    
    protected virtual void UniqueBuildingsCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs args) {
    }
    
    protected virtual void TilesCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs args) {
    }
}

public partial class FieldManagerViewModel : FieldManagerViewModelBase {
    
    public FieldManagerViewModel(FieldManagerControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public FieldManagerViewModel() : 
            base() {
    }
    
    public virtual P<CommonBuildingViewModel> CurrentCommonBuildingProperty {
        get {
            return this._CurrentCommonBuildingProperty;
        }
    }
    
    public virtual CommonBuildingViewModel CurrentCommonBuilding {
        get {
            return _CurrentCommonBuildingProperty.Value;
        }
        set {
            _CurrentCommonBuildingProperty.Value = value;
            if (value != null) value.ParentFieldManager = this;
        }
    }
    
    public virtual P<CommonBuildingViewModel> OldCommonBuildingProperty {
        get {
            return this._OldCommonBuildingProperty;
        }
    }
    
    public virtual CommonBuildingViewModel OldCommonBuilding {
        get {
            return _OldCommonBuildingProperty.Value;
        }
        set {
            _OldCommonBuildingProperty.Value = value;
            if (value != null) value.ParentFieldManager = this;
        }
    }
    
    public virtual P<UniqueBuildingViewModel> CurrentUniqueBuildingProperty {
        get {
            return this._CurrentUniqueBuildingProperty;
        }
    }
    
    public virtual UniqueBuildingViewModel CurrentUniqueBuilding {
        get {
            return _CurrentUniqueBuildingProperty.Value;
        }
        set {
            _CurrentUniqueBuildingProperty.Value = value;
            if (value != null) value.ParentFieldManager = this;
        }
    }
    
    public virtual P<UniqueBuildingViewModel> OldUniqueBuildingProperty {
        get {
            return this._OldUniqueBuildingProperty;
        }
    }
    
    public virtual UniqueBuildingViewModel OldUniqueBuilding {
        get {
            return _OldUniqueBuildingProperty.Value;
        }
        set {
            _OldUniqueBuildingProperty.Value = value;
            if (value != null) value.ParentFieldManager = this;
        }
    }
    
    public virtual P<TileViewModel> CurrentSelectedTileProperty {
        get {
            return this._CurrentSelectedTileProperty;
        }
    }
    
    public virtual TileViewModel CurrentSelectedTile {
        get {
            return _CurrentSelectedTileProperty.Value;
        }
        set {
            _CurrentSelectedTileProperty.Value = value;
            if (value != null) value.ParentFieldManager = this;
        }
    }
    
    public virtual P<TileViewModel> OldSelectedTileProperty {
        get {
            return this._OldSelectedTileProperty;
        }
    }
    
    public virtual TileViewModel OldSelectedTile {
        get {
            return _OldSelectedTileProperty.Value;
        }
        set {
            _OldSelectedTileProperty.Value = value;
            if (value != null) value.ParentFieldManager = this;
        }
    }
    
    public virtual P<Int32> TileCountLineProperty {
        get {
            return this._TileCountLineProperty;
        }
    }
    
    public virtual Int32 TileCountLine {
        get {
            return _TileCountLineProperty.Value;
        }
        set {
            _TileCountLineProperty.Value = value;
        }
    }
    
    public virtual P<Int32> TileCountColumnProperty {
        get {
            return this._TileCountColumnProperty;
        }
    }
    
    public virtual Int32 TileCountColumn {
        get {
            return _TileCountColumnProperty.Value;
        }
        set {
            _TileCountColumnProperty.Value = value;
        }
    }
    
    public virtual ModelCollection<CommonBuildingViewModel> CommonBuildingButtons {
        get {
            return this._CommonBuildingButtonsProperty;
        }
    }
    
    public virtual ModelCollection<BuildingViewModel> CommonBuildings {
        get {
            return this._CommonBuildingsProperty;
        }
    }
    
    public virtual ModelCollection<UniqueBuildingViewModel> UniqueBuildingButtons {
        get {
            return this._UniqueBuildingButtonsProperty;
        }
    }
    
    public virtual ModelCollection<BuildingViewModel> UniqueBuildings {
        get {
            return this._UniqueBuildingsProperty;
        }
    }
    
    public virtual ModelCollection<TileViewModel> Tiles {
        get {
            return this._TilesProperty;
        }
    }
    
    public virtual CommandWithSender<FieldManagerViewModel> SetupField {
        get {
            return _SetupField;
        }
        set {
            _SetupField = value;
        }
    }
    
    public virtual CommandWithSenderAndArgument<FieldManagerViewModel, CommonBuildingViewModel> InstantiateCommonBuilding {
        get {
            return _InstantiateCommonBuilding;
        }
        set {
            _InstantiateCommonBuilding = value;
        }
    }
    
    public virtual CommandWithSenderAndArgument<FieldManagerViewModel, UniqueBuildingViewModel> InstantiateUniqueBuilding {
        get {
            return _InstantiateUniqueBuilding;
        }
        set {
            _InstantiateUniqueBuilding = value;
        }
    }
    
    public virtual CommandWithSender<FieldManagerViewModel> Save {
        get {
            return _Save;
        }
        set {
            _Save = value;
        }
    }
    
    public virtual CommandWithSender<FieldManagerViewModel> Load {
        get {
            return _Load;
        }
        set {
            _Load = value;
        }
    }
    
    protected override void WireCommands(Controller controller) {
        var fieldManager = controller as FieldManagerControllerBase;
        this.SetupField = new CommandWithSender<FieldManagerViewModel>(this, fieldManager.SetupField);
        this.InstantiateCommonBuilding = new CommandWithSenderAndArgument<FieldManagerViewModel, CommonBuildingViewModel>(this, fieldManager.InstantiateCommonBuilding);
        this.InstantiateUniqueBuilding = new CommandWithSenderAndArgument<FieldManagerViewModel, UniqueBuildingViewModel>(this, fieldManager.InstantiateUniqueBuilding);
        this.Save = new CommandWithSender<FieldManagerViewModel>(this, fieldManager.Save);
        this.Load = new CommandWithSender<FieldManagerViewModel>(this, fieldManager.Load);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
		if (stream.DeepSerialize) stream.SerializeObject("CurrentCommonBuilding", this.CurrentCommonBuilding);
		if (stream.DeepSerialize) stream.SerializeObject("OldCommonBuilding", this.OldCommonBuilding);
		if (stream.DeepSerialize) stream.SerializeObject("CurrentUniqueBuilding", this.CurrentUniqueBuilding);
		if (stream.DeepSerialize) stream.SerializeObject("OldUniqueBuilding", this.OldUniqueBuilding);
		if (stream.DeepSerialize) stream.SerializeObject("CurrentSelectedTile", this.CurrentSelectedTile);
		if (stream.DeepSerialize) stream.SerializeObject("OldSelectedTile", this.OldSelectedTile);
        stream.SerializeInt("TileCountLine", this.TileCountLine);
        stream.SerializeInt("TileCountColumn", this.TileCountColumn);
        if (stream.DeepSerialize) stream.SerializeArray("CommonBuildingButtons", this.CommonBuildingButtons);
        if (stream.DeepSerialize) stream.SerializeArray("CommonBuildings", this.CommonBuildings);
        if (stream.DeepSerialize) stream.SerializeArray("UniqueBuildingButtons", this.UniqueBuildingButtons);
        if (stream.DeepSerialize) stream.SerializeArray("UniqueBuildings", this.UniqueBuildings);
        if (stream.DeepSerialize) stream.SerializeArray("Tiles", this.Tiles);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
		if (stream.DeepSerialize) this.CurrentCommonBuilding = stream.DeserializeObject<CommonBuildingViewModel>("CurrentCommonBuilding");
		if (stream.DeepSerialize) this.OldCommonBuilding = stream.DeserializeObject<CommonBuildingViewModel>("OldCommonBuilding");
		if (stream.DeepSerialize) this.CurrentUniqueBuilding = stream.DeserializeObject<UniqueBuildingViewModel>("CurrentUniqueBuilding");
		if (stream.DeepSerialize) this.OldUniqueBuilding = stream.DeserializeObject<UniqueBuildingViewModel>("OldUniqueBuilding");
		if (stream.DeepSerialize) this.CurrentSelectedTile = stream.DeserializeObject<TileViewModel>("CurrentSelectedTile");
		if (stream.DeepSerialize) this.OldSelectedTile = stream.DeserializeObject<TileViewModel>("OldSelectedTile");
        		this.TileCountLine = stream.DeserializeInt("TileCountLine");;
        		this.TileCountColumn = stream.DeserializeInt("TileCountColumn");;
if (stream.DeepSerialize) {
        this.CommonBuildingButtons.Clear();
        this.CommonBuildingButtons.AddRange(stream.DeserializeObjectArray<CommonBuildingViewModel>("CommonBuildingButtons"));
}
if (stream.DeepSerialize) {
        this.CommonBuildings.Clear();
        this.CommonBuildings.AddRange(stream.DeserializeObjectArray<BuildingViewModel>("CommonBuildings"));
}
if (stream.DeepSerialize) {
        this.UniqueBuildingButtons.Clear();
        this.UniqueBuildingButtons.AddRange(stream.DeserializeObjectArray<UniqueBuildingViewModel>("UniqueBuildingButtons"));
}
if (stream.DeepSerialize) {
        this.UniqueBuildings.Clear();
        this.UniqueBuildings.AddRange(stream.DeserializeObjectArray<BuildingViewModel>("UniqueBuildings"));
}
if (stream.DeepSerialize) {
        this.Tiles.Clear();
        this.Tiles.AddRange(stream.DeserializeObjectArray<TileViewModel>("Tiles"));
}
    }
    
    public override void Unbind() {
        base.Unbind();
        _CommonBuildingButtonsProperty.CollectionChanged -= CommonBuildingButtonsCollectionChanged;
        _CommonBuildingsProperty.CollectionChanged -= CommonBuildingsCollectionChanged;
        _UniqueBuildingButtonsProperty.CollectionChanged -= UniqueBuildingButtonsCollectionChanged;
        _UniqueBuildingsProperty.CollectionChanged -= UniqueBuildingsCollectionChanged;
        _TilesProperty.CollectionChanged -= TilesCollectionChanged;
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
        list.Add(new ViewModelPropertyInfo(_CurrentCommonBuildingProperty, true, false, false));
        list.Add(new ViewModelPropertyInfo(_OldCommonBuildingProperty, true, false, false));
        list.Add(new ViewModelPropertyInfo(_CurrentUniqueBuildingProperty, true, false, false));
        list.Add(new ViewModelPropertyInfo(_OldUniqueBuildingProperty, true, false, false));
        list.Add(new ViewModelPropertyInfo(_CurrentSelectedTileProperty, true, false, false));
        list.Add(new ViewModelPropertyInfo(_OldSelectedTileProperty, true, false, false));
        list.Add(new ViewModelPropertyInfo(_TileCountLineProperty, false, false, false));
        list.Add(new ViewModelPropertyInfo(_TileCountColumnProperty, false, false, false));
        list.Add(new ViewModelPropertyInfo(_CommonBuildingButtonsProperty, true, true, false));
        list.Add(new ViewModelPropertyInfo(_CommonBuildingsProperty, true, true, false));
        list.Add(new ViewModelPropertyInfo(_UniqueBuildingButtonsProperty, true, true, false));
        list.Add(new ViewModelPropertyInfo(_UniqueBuildingsProperty, true, true, false));
        list.Add(new ViewModelPropertyInfo(_TilesProperty, true, true, false));
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
        list.Add(new ViewModelCommandInfo("SetupField", SetupField) { ParameterType = typeof(void) });
        list.Add(new ViewModelCommandInfo("InstantiateCommonBuilding", InstantiateCommonBuilding) { ParameterType = typeof(CommonBuildingViewModel) });
        list.Add(new ViewModelCommandInfo("InstantiateUniqueBuilding", InstantiateUniqueBuilding) { ParameterType = typeof(UniqueBuildingViewModel) });
        list.Add(new ViewModelCommandInfo("Save", Save) { ParameterType = typeof(void) });
        list.Add(new ViewModelCommandInfo("Load", Load) { ParameterType = typeof(void) });
    }
    
    protected override void CommonBuildingButtonsCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs args) {
        foreach (var item in args.NewItems.OfType<CommonBuildingViewModel>()) item.ParentFieldManager = this;;
    }
    
    protected override void CommonBuildingsCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs args) {
        foreach (var item in args.NewItems.OfType<BuildingViewModel>()) item.ParentFieldManager = this;;
    }
    
    protected override void UniqueBuildingButtonsCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs args) {
        foreach (var item in args.NewItems.OfType<UniqueBuildingViewModel>()) item.ParentFieldManager = this;;
    }
    
    protected override void UniqueBuildingsCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs args) {
        foreach (var item in args.NewItems.OfType<BuildingViewModel>()) item.ParentFieldManager = this;;
    }
    
    protected override void TilesCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs args) {
        foreach (var item in args.NewItems.OfType<TileViewModel>()) item.ParentFieldManager = this;;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class TileViewModelBase : ViewModel {
    
    public P<Int32> _IdProperty;
    
    public P<Boolean> _IsEmptyProperty;
    
    public P<Vector2> _IndexProperty;
    
    public P<Boolean> _IsSelectedProperty;
    
    public TileViewModelBase(TileControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public TileViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
        _IdProperty = new P<Int32>(this, "Id");
        _IsEmptyProperty = new P<Boolean>(this, "IsEmpty");
        _IndexProperty = new P<Vector2>(this, "Index");
        _IsSelectedProperty = new P<Boolean>(this, "IsSelected");
    }
}

public partial class TileViewModel : TileViewModelBase {
    
    private FieldManagerViewModel _ParentFieldManager;
    
    public TileViewModel(TileControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public TileViewModel() : 
            base() {
    }
    
    public virtual P<Int32> IdProperty {
        get {
            return this._IdProperty;
        }
    }
    
    public virtual Int32 Id {
        get {
            return _IdProperty.Value;
        }
        set {
            _IdProperty.Value = value;
        }
    }
    
    public virtual P<Boolean> IsEmptyProperty {
        get {
            return this._IsEmptyProperty;
        }
    }
    
    public virtual Boolean IsEmpty {
        get {
            return _IsEmptyProperty.Value;
        }
        set {
            _IsEmptyProperty.Value = value;
        }
    }
    
    public virtual P<Vector2> IndexProperty {
        get {
            return this._IndexProperty;
        }
    }
    
    public virtual Vector2 Index {
        get {
            return _IndexProperty.Value;
        }
        set {
            _IndexProperty.Value = value;
        }
    }
    
    public virtual P<Boolean> IsSelectedProperty {
        get {
            return this._IsSelectedProperty;
        }
    }
    
    public virtual Boolean IsSelected {
        get {
            return _IsSelectedProperty.Value;
        }
        set {
            _IsSelectedProperty.Value = value;
        }
    }
    
    public virtual FieldManagerViewModel ParentFieldManager {
        get {
            return this._ParentFieldManager;
        }
        set {
            _ParentFieldManager = value;
        }
    }
    
    protected override void WireCommands(Controller controller) {
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
        stream.SerializeInt("Id", this.Id);
        stream.SerializeBool("IsEmpty", this.IsEmpty);
        stream.SerializeVector2("Index", this.Index);
        stream.SerializeBool("IsSelected", this.IsSelected);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
        		this.Id = stream.DeserializeInt("Id");;
        		this.IsEmpty = stream.DeserializeBool("IsEmpty");;
        		this.Index = stream.DeserializeVector2("Index");;
        		this.IsSelected = stream.DeserializeBool("IsSelected");;
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
        list.Add(new ViewModelPropertyInfo(_IdProperty, false, false, false));
        list.Add(new ViewModelPropertyInfo(_IsEmptyProperty, false, false, false));
        list.Add(new ViewModelPropertyInfo(_IndexProperty, false, false, false));
        list.Add(new ViewModelPropertyInfo(_IsSelectedProperty, false, false, false));
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class UniqueBuildingViewModelBase : BuildingViewModel {
    
    public P<UniqueBuildingType> _TypeProperty;
    
    public P<Int32> _IdProperty;
    
    public P<Boolean> _IsAddedProperty;
    
    public P<Boolean> _IsSelectedProperty;
    
    public P<Boolean> _IsDeletedProperty;
    
    protected CommandWithSender<UniqueBuildingViewModel> _Delete;
    
    public UniqueBuildingViewModelBase(UniqueBuildingControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public UniqueBuildingViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
        _TypeProperty = new P<UniqueBuildingType>(this, "Type");
        _IdProperty = new P<Int32>(this, "Id");
        _IsAddedProperty = new P<Boolean>(this, "IsAdded");
        _IsSelectedProperty = new P<Boolean>(this, "IsSelected");
        _IsDeletedProperty = new P<Boolean>(this, "IsDeleted");
    }
}

public partial class UniqueBuildingViewModel : UniqueBuildingViewModelBase {
    
    public UniqueBuildingViewModel(UniqueBuildingControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public UniqueBuildingViewModel() : 
            base() {
    }
    
    public virtual P<UniqueBuildingType> TypeProperty {
        get {
            return this._TypeProperty;
        }
    }
    
    public virtual UniqueBuildingType Type {
        get {
            return _TypeProperty.Value;
        }
        set {
            _TypeProperty.Value = value;
        }
    }
    
    public virtual P<Int32> IdProperty {
        get {
            return this._IdProperty;
        }
    }
    
    public virtual Int32 Id {
        get {
            return _IdProperty.Value;
        }
        set {
            _IdProperty.Value = value;
        }
    }
    
    public virtual P<Boolean> IsAddedProperty {
        get {
            return this._IsAddedProperty;
        }
    }
    
    public virtual Boolean IsAdded {
        get {
            return _IsAddedProperty.Value;
        }
        set {
            _IsAddedProperty.Value = value;
        }
    }
    
    public virtual P<Boolean> IsSelectedProperty {
        get {
            return this._IsSelectedProperty;
        }
    }
    
    public virtual Boolean IsSelected {
        get {
            return _IsSelectedProperty.Value;
        }
        set {
            _IsSelectedProperty.Value = value;
        }
    }
    
    public virtual P<Boolean> IsDeletedProperty {
        get {
            return this._IsDeletedProperty;
        }
    }
    
    public virtual Boolean IsDeleted {
        get {
            return _IsDeletedProperty.Value;
        }
        set {
            _IsDeletedProperty.Value = value;
        }
    }
    
    public virtual CommandWithSender<UniqueBuildingViewModel> Delete {
        get {
            return _Delete;
        }
        set {
            _Delete = value;
        }
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
        var uniqueBuilding = controller as UniqueBuildingControllerBase;
        this.Delete = new CommandWithSender<UniqueBuildingViewModel>(this, uniqueBuilding.Delete);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
		stream.SerializeInt("Type", (int)this.Type);
        stream.SerializeInt("Id", this.Id);
        stream.SerializeBool("IsAdded", this.IsAdded);
        stream.SerializeBool("IsSelected", this.IsSelected);
        stream.SerializeBool("IsDeleted", this.IsDeleted);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
		this.Type = (UniqueBuildingType)stream.DeserializeInt("Type");
        		this.Id = stream.DeserializeInt("Id");;
        		this.IsAdded = stream.DeserializeBool("IsAdded");;
        		this.IsSelected = stream.DeserializeBool("IsSelected");;
        		this.IsDeleted = stream.DeserializeBool("IsDeleted");;
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
        list.Add(new ViewModelPropertyInfo(_TypeProperty, false, false, true));
        list.Add(new ViewModelPropertyInfo(_IdProperty, false, false, false));
        list.Add(new ViewModelPropertyInfo(_IsAddedProperty, false, false, false));
        list.Add(new ViewModelPropertyInfo(_IsSelectedProperty, false, false, false));
        list.Add(new ViewModelPropertyInfo(_IsDeletedProperty, false, false, false));
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
        list.Add(new ViewModelCommandInfo("Delete", Delete) { ParameterType = typeof(void) });
    }
}

[DiagramInfoAttribute("CityDemo")]
public class CommonBuildingViewModelBase : BuildingViewModel {
    
    public P<CommonBuildingType> _TypeProperty;
    
    public P<Boolean> _IsSelectedProperty;
    
    public CommonBuildingViewModelBase(CommonBuildingControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public CommonBuildingViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
        _TypeProperty = new P<CommonBuildingType>(this, "Type");
        _IsSelectedProperty = new P<Boolean>(this, "IsSelected");
    }
}

public partial class CommonBuildingViewModel : CommonBuildingViewModelBase {
    
    public CommonBuildingViewModel(CommonBuildingControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public CommonBuildingViewModel() : 
            base() {
    }
    
    public virtual P<CommonBuildingType> TypeProperty {
        get {
            return this._TypeProperty;
        }
    }
    
    public virtual CommonBuildingType Type {
        get {
            return _TypeProperty.Value;
        }
        set {
            _TypeProperty.Value = value;
        }
    }
    
    public virtual P<Boolean> IsSelectedProperty {
        get {
            return this._IsSelectedProperty;
        }
    }
    
    public virtual Boolean IsSelected {
        get {
            return _IsSelectedProperty.Value;
        }
        set {
            _IsSelectedProperty.Value = value;
        }
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
		stream.SerializeInt("Type", (int)this.Type);
        stream.SerializeBool("IsSelected", this.IsSelected);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
		this.Type = (CommonBuildingType)stream.DeserializeInt("Type");
        		this.IsSelected = stream.DeserializeBool("IsSelected");;
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
        list.Add(new ViewModelPropertyInfo(_TypeProperty, false, false, true));
        list.Add(new ViewModelPropertyInfo(_IsSelectedProperty, false, false, false));
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class BarrackViewModelBase : CommonBuildingViewModel {
    
    protected CommandWithSender<BarrackViewModel> _CreateSoldier;
    
    public BarrackViewModelBase(BarrackControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public BarrackViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class BarrackViewModel : BarrackViewModelBase {
    
    public BarrackViewModel(BarrackControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public BarrackViewModel() : 
            base() {
    }
    
    public virtual CommandWithSender<BarrackViewModel> CreateSoldier {
        get {
            return _CreateSoldier;
        }
        set {
            _CreateSoldier = value;
        }
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
        var barrack = controller as BarrackControllerBase;
        this.CreateSoldier = new CommandWithSender<BarrackViewModel>(this, barrack.CreateSoldier);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
        list.Add(new ViewModelCommandInfo("CreateSoldier", CreateSoldier) { ParameterType = typeof(void) });
    }
}

[DiagramInfoAttribute("CityDemo")]
public class TowerViewModelBase : CommonBuildingViewModel {
    
    public TowerViewModelBase(TowerControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public TowerViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class TowerViewModel : TowerViewModelBase {
    
    public TowerViewModel(TowerControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public TowerViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class HomeViewModelBase : CommonBuildingViewModel {
    
    public HomeViewModelBase(HomeControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public HomeViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class HomeViewModel : HomeViewModelBase {
    
    public HomeViewModel(HomeControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public HomeViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class FarmViewModelBase : CommonBuildingViewModel {
    
    public FarmViewModelBase(FarmControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public FarmViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class FarmViewModel : FarmViewModelBase {
    
    public FarmViewModel(FarmControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public FarmViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class WallViewModelBase : CommonBuildingViewModel {
    
    public WallViewModelBase(WallControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public WallViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class WallViewModel : WallViewModelBase {
    
    public WallViewModel(WallControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public WallViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class ElfTownViewModelBase : UniqueBuildingViewModel {
    
    public ElfTownViewModelBase(ElfTownControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public ElfTownViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class ElfTownViewModel : ElfTownViewModelBase {
    
    public ElfTownViewModel(ElfTownControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public ElfTownViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class OrcTownViewModelBase : UniqueBuildingViewModel {
    
    public OrcTownViewModelBase(OrcTownControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public OrcTownViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class OrcTownViewModel : OrcTownViewModelBase {
    
    public OrcTownViewModel(OrcTownControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public OrcTownViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class HumanTownViewModelBase : UniqueBuildingViewModel {
    
    public HumanTownViewModelBase(HumanTownControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public HumanTownViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class HumanTownViewModel : HumanTownViewModelBase {
    
    public HumanTownViewModel(HumanTownControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public HumanTownViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class GamerTownViewModelBase : UniqueBuildingViewModel {
    
    public GamerTownViewModelBase(GamerTownControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public GamerTownViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class GamerTownViewModel : GamerTownViewModelBase {
    
    public GamerTownViewModel(GamerTownControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public GamerTownViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class MagicForestViewModelBase : UniqueBuildingViewModel {
    
    public MagicForestViewModelBase(MagicForestControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public MagicForestViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class MagicForestViewModel : MagicForestViewModelBase {
    
    public MagicForestViewModel(MagicForestControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public MagicForestViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class KnightsHallViewModelBase : UniqueBuildingViewModel {
    
    public KnightsHallViewModelBase(KnightsHallControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public KnightsHallViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class KnightsHallViewModel : KnightsHallViewModelBase {
    
    public KnightsHallViewModel(KnightsHallControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public KnightsHallViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class ObserverEyeViewModelBase : UniqueBuildingViewModel {
    
    public ObserverEyeViewModelBase(ObserverEyeControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public ObserverEyeViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class ObserverEyeViewModel : ObserverEyeViewModelBase {
    
    public ObserverEyeViewModel(ObserverEyeControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public ObserverEyeViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class PotterGeneratorViewModelBase : UniqueBuildingViewModel {
    
    public PotterGeneratorViewModelBase(PotterGeneratorControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public PotterGeneratorViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class PotterGeneratorViewModel : PotterGeneratorViewModelBase {
    
    public PotterGeneratorViewModel(PotterGeneratorControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public PotterGeneratorViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class MonsterSlapperViewModelBase : UniqueBuildingViewModel {
    
    public MonsterSlapperViewModelBase(MonsterSlapperControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public MonsterSlapperViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class MonsterSlapperViewModel : MonsterSlapperViewModelBase {
    
    public MonsterSlapperViewModel(MonsterSlapperControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public MonsterSlapperViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class GreenBankViewModelBase : UniqueBuildingViewModel {
    
    public GreenBankViewModelBase(GreenBankControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public GreenBankViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class GreenBankViewModel : GreenBankViewModelBase {
    
    public GreenBankViewModel(GreenBankControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public GreenBankViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class SwordGeneratorViewModelBase : UniqueBuildingViewModel {
    
    public SwordGeneratorViewModelBase(SwordGeneratorControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public SwordGeneratorViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class SwordGeneratorViewModel : SwordGeneratorViewModelBase {
    
    public SwordGeneratorViewModel(SwordGeneratorControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public SwordGeneratorViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class DragoonSlayerViewModelBase : UniqueBuildingViewModel {
    
    public DragoonSlayerViewModelBase(DragoonSlayerControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public DragoonSlayerViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class DragoonSlayerViewModel : DragoonSlayerViewModelBase {
    
    public DragoonSlayerViewModel(DragoonSlayerControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public DragoonSlayerViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class WiseManGeneratorViewModelBase : UniqueBuildingViewModel {
    
    public WiseManGeneratorViewModelBase(WiseManGeneratorControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public WiseManGeneratorViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class WiseManGeneratorViewModel : WiseManGeneratorViewModelBase {
    
    public WiseManGeneratorViewModel(WiseManGeneratorControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public WiseManGeneratorViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class EarthShakerViewModelBase : UniqueBuildingViewModel {
    
    public EarthShakerViewModelBase(EarthShakerControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public EarthShakerViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class EarthShakerViewModel : EarthShakerViewModelBase {
    
    public EarthShakerViewModel(EarthShakerControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public EarthShakerViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

[DiagramInfoAttribute("CityDemo")]
public class MagicShieldViewModelBase : UniqueBuildingViewModel {
    
    public MagicShieldViewModelBase(MagicShieldControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public MagicShieldViewModelBase() : 
            base() {
    }
    
    public override void Bind() {
        base.Bind();
    }
}

public partial class MagicShieldViewModel : MagicShieldViewModelBase {
    
    public MagicShieldViewModel(MagicShieldControllerBase controller, bool initialize = true) : 
            base(controller, initialize) {
    }
    
    public MagicShieldViewModel() : 
            base() {
    }
    
    protected override void WireCommands(Controller controller) {
        base.WireCommands(controller);
    }
    
    public override void Write(ISerializerStream stream) {
		base.Write(stream);
    }
    
    public override void Read(ISerializerStream stream) {
		base.Read(stream);
    }
    
    public override void Unbind() {
        base.Unbind();
    }
    
    protected override void FillProperties(List<ViewModelPropertyInfo> list) {
        base.FillProperties(list);;
    }
    
    protected override void FillCommands(List<ViewModelCommandInfo> list) {
        base.FillCommands(list);;
    }
}

public enum UniqueBuildingType {
    
    ElfTown,
    
    OrcTown,
    
    HumanTown,
    
    GamerTown,
    
    MagicForest,
    
    KnightsHall,
    
    ObserverEye,
    
    PotterGenerator,
    
    MonsterSlapper,
    
    GreenBank,
    
    SwordGenerator,
    
    DragoonSlayer,
    
    WiseManGenerator,
    
    EarthShaker,
    
    MagicShield,
}

public enum CommonBuildingType {
    
    Barrack,
    
    Tower,
    
    Home,
    
    Farm,
    
    Wall,
}
