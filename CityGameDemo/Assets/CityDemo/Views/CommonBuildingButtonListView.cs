using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public partial class CommonBuildingButtonListView { 


    //Common building button prefabs
    public GameObject BarrackBuildingButton;
    public GameObject TowerBuildingButton;
    public GameObject HomeBuildingButton;
    public GameObject FarmBuildingButton;
    public GameObject WallBuildingButton;

    private List<CommonBuildingViewModel> _commonBuildingButtonData;


    public override void Awake()
    {
        base.Awake();

        InitButtonData();
        CreateAllButtons();
    }

    //initiate button data
    private void InitButtonData()
    {
        _commonBuildingButtonData = new List<CommonBuildingViewModel>();
        //button virtual list created start
        var commonBuildingButtonData = new CommonBuildingButtonData();
        //var uniqueBuildingButtonData = new UniqueBuildingButtonData();

        _commonBuildingButtonData = commonBuildingButtonData.GetButtonData();
        //_uniqueBuildingButtonData = uniqueBuildingButtonData.GetButtonData();
    }

    private void CreateAllButtons()
    {
        for (int i = 0; i < _commonBuildingButtonData.Count; i++)
        {
            FieldManager.CommonBuildingButtons.Add(_commonBuildingButtonData[i]);
        }
    }

    /// Subscribes to collection modifications.  Add & Remove methods are invoked for each modification.
    public override void CommonBuildingButtonsAdded(CommonBuildingViewModel item)
    {
        base.CommonBuildingButtonsAdded(item);
        CreateCommmonBuildingButton(item);
    }

    /// Subscribes to collection modifications.  Add & Remove methods are invoked for each modification.
    public override void CommonBuildingButtonsRemoved(CommonBuildingViewModel item)
    {
        base.CommonBuildingButtonsRemoved(item);
    }

    /// creates the common building buttons
    private void CreateCommmonBuildingButton(CommonBuildingViewModel item)
    {
        switch (item.Type)
        {
            case CommonBuildingType.Barrack:
                var barrackButton = Instantiate(BarrackBuildingButton) as GameObject;
                barrackButton.GetComponent<BarrackButtonView>().Barrack.Type = CommonBuildingType.Barrack;

                AddTriggerToButton(barrackButton, barrackButton.GetComponent<BarrackButtonView>().CommonBuilding);
                barrackButton.transform.parent = transform;
                break;

            case CommonBuildingType.Tower:
                var towerButton = Instantiate(TowerBuildingButton) as GameObject;
                towerButton.GetComponent<TowerButtonView>().Tower.Type = CommonBuildingType.Tower;

                AddTriggerToButton(towerButton, towerButton.GetComponent<TowerButtonView>().CommonBuilding);
                towerButton.transform.parent = transform;
                break;

            case CommonBuildingType.Home:
                var homeButton = Instantiate(HomeBuildingButton) as GameObject;
                homeButton.GetComponent<HomeButtonView>().Home.Type = CommonBuildingType.Home;

                AddTriggerToButton(homeButton, homeButton.GetComponent<HomeButtonView>().CommonBuilding);
                homeButton.transform.parent = transform;
                break;

            case CommonBuildingType.Farm:
                var farmButton = Instantiate(FarmBuildingButton) as GameObject;
                farmButton.GetComponent<FarmButtonView>().Farm.Type = CommonBuildingType.Farm;

                AddTriggerToButton(farmButton, farmButton.GetComponent<FarmButtonView>().CommonBuilding);
                farmButton.transform.parent = transform;
                break;

            case CommonBuildingType.Wall:
                var wallButton = Instantiate(WallBuildingButton) as GameObject;
                wallButton.GetComponent<WallButtonView>().Wall.Type = CommonBuildingType.Wall;

                AddTriggerToButton(wallButton, wallButton.GetComponent<WallButtonView>().CommonBuilding);
                wallButton.transform.parent = transform;
                break;
            default:
                break;
        }
    }

    private void AddTriggerToButton(GameObject go, CommonBuildingViewModel commonBuildingViewModel)
    {
        EventTrigger trigger = go.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;

        entry.callback.AddListener((eventData) =>
        {
            ExecuteInstantiateCommonBuilding(commonBuildingViewModel);
        });

        trigger.triggers.Add(entry);
    }


    private class CommonBuildingButtonData
    {
        public List<CommonBuildingViewModel> BuildingModelsData;

        public CommonBuildingButtonData()
        {

            BuildingModelsData = new List<CommonBuildingViewModel>();
            //1. bina modeli
            CommonBuildingViewModel barrackModel = new CommonBuildingViewModel();
            barrackModel.Type = CommonBuildingType.Barrack;

            CommonBuildingViewModel wallModel = new CommonBuildingViewModel();
            wallModel.Type = CommonBuildingType.Wall;

            CommonBuildingViewModel towerModel = new CommonBuildingViewModel();
            towerModel.Type = CommonBuildingType.Tower;

            CommonBuildingViewModel homeModel = new CommonBuildingViewModel();
            homeModel.Type = CommonBuildingType.Home;

            CommonBuildingViewModel farmModel = new CommonBuildingViewModel();
            farmModel.Type = CommonBuildingType.Farm;

            //buildingModel1.TypeName = "Yap� A";
            //buildingModel1.ButonText = "Yap� A";
            //buildingModel1.ModelType = ModelType.Building;
            //buildingModel1.Type = BuildingType.Bagdadi;
            //buildingModel1.Deleted = false;
            //buildingModel1.Rotation = 0f;

            
            BuildingModelsData.Add(barrackModel);
            BuildingModelsData.Add(wallModel);
            BuildingModelsData.Add(homeModel);
            BuildingModelsData.Add(towerModel);
            BuildingModelsData.Add(farmModel);
        }

        public List<CommonBuildingViewModel> GetButtonData()
        {
            return BuildingModelsData;
        }
    }

}
