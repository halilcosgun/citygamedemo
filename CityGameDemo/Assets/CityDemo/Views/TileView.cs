using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;


public partial class TileView
{
    private SpriteRenderer _spriteRenderer;
    /// Subscribes to the property and is notified anytime the value changes.
    public override void IsSelectedChanged(Boolean isSelected) {
        base.IsSelectedChanged(isSelected);

        if (isSelected)
        {
            _spriteRenderer.color = Color.green;
        }
        else
        {
            _spriteRenderer.color = Color.white;
        }
        
    }

    public override void Awake()
    {
        base.Awake();
        Tile.IsEmpty = true;
        Tile.Index = transform.position;
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
}
