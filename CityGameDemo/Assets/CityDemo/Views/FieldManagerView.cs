using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;


public partial class FieldManagerView
{ 
    //Common Building Prefabs
    public GameObject Barrack;
    public GameObject Home;
    public GameObject Wall;
    public GameObject Tower;
    public GameObject Farm;
    public Transform CommonBuildingsParent;

    //Unique Building Prefabs
    public GameObject ElfTown;
    public GameObject OrcTown;
    public GameObject HumanTown;
    public GameObject GamerTown;
    public GameObject MagicForest;
    public GameObject KnightsHall;
    public GameObject SwordGenerator;
    public GameObject GreenBank;
    public GameObject ObserverEye;
    public GameObject PotterGenerator;
    public GameObject DragoonSlayer;
    public GameObject MonsterSlapper;
    public GameObject MagicShield;
    public GameObject WiseManGenerator;
    public GameObject EarthShaker;
    public Transform UniqueBuildingParent;

    public LayerMask FieldLayerMask;
    public LayerMask UniqueBuildingLayerMask;
    public LayerMask CommonBuildingLayerMask;

    public override void Awake()
    {
        base.Awake();
        ExecuteLoad();
    }

    /// Subscribes to collection modifications.  Add & Remove methods are invoked for each modification.
    public override void CommonBuildingsAdded(BuildingViewModel item) {
        base.CommonBuildingsAdded(item);
        CreateCommmonBuilding((CommonBuildingViewModel)item);
    }

    private void CreateCommmonBuilding(CommonBuildingViewModel commonBuilding)
    {
            switch (commonBuilding.Type)
            {
                case CommonBuildingType.Barrack:
                    var barrack = Instantiate(Barrack) as GameObject;
                    var barrackModel = barrack.GetComponent<CommonBuildingView>().CommonBuilding;
                    barrackModel.Type = commonBuilding.Type;
                    barrackModel.Position = commonBuilding.Position;
                    barrackModel.IsSelected = commonBuilding.IsSelected;
                    barrack.transform.parent = CommonBuildingsParent;
                    SetCommonBuildingPosition(barrack.transform, commonBuilding);
                    break;
                case CommonBuildingType.Tower:
                    var tower = Instantiate(Tower) as GameObject;
                    var towerModel = tower.GetComponent<CommonBuildingView>().CommonBuilding;
                    towerModel.Type = commonBuilding.Type;
                    towerModel.Position = commonBuilding.Position;
                    towerModel.IsSelected = commonBuilding.IsSelected;
                    tower.transform.parent = CommonBuildingsParent;
                    SetCommonBuildingPosition(tower.transform, commonBuilding);
                    break;
                case CommonBuildingType.Home:
                    var home = Instantiate(Home) as GameObject;
                    var homeModel = home.GetComponent<CommonBuildingView>().CommonBuilding;
                    homeModel.Type = commonBuilding.Type;
                    homeModel.IsSelected = commonBuilding.IsSelected;
                    homeModel.Position = commonBuilding.Position;
                    home.transform.parent = CommonBuildingsParent;
                    SetCommonBuildingPosition(home.transform, commonBuilding);
                    break;
                case CommonBuildingType.Farm:
                    var farm = Instantiate(Farm) as GameObject;
                    var farmModel = farm.GetComponent<CommonBuildingView>().CommonBuilding;
                    farmModel.Type = commonBuilding.Type;
                    farmModel.IsSelected = commonBuilding.IsSelected;
                    farmModel.Position = commonBuilding.Position;
                    farm.transform.parent = CommonBuildingsParent;
                    SetCommonBuildingPosition(farm.transform, commonBuilding);
                    break;
                case CommonBuildingType.Wall:
                    var wall = Instantiate(Wall) as GameObject;
                var wallModel = wall.GetComponent<CommonBuildingView>().CommonBuilding;
                    wallModel.Type = commonBuilding.Type;
                    wallModel.IsSelected = commonBuilding.IsSelected;
                    wallModel.Position = commonBuilding.Position;
                    wall.transform.parent = CommonBuildingsParent;
                    SetCommonBuildingPosition(wall.transform, commonBuilding);
                    break;
                default:
                    break;
            }
            
        
        
    }

    private void SetCommonBuildingPosition(Transform buildingTransform, CommonBuildingViewModel commonBuildingModel)
    {
        if (FieldManager.CurrentSelectedTile == null)
        {
            buildingTransform.position = commonBuildingModel.Position;
        }
        else
        {
            buildingTransform.position = FieldManager.CurrentSelectedTile.Index;
            FieldManager.CurrentSelectedTile.IsEmpty = false;
        }
        
    }

    private void SetUniqueBuildingPosition(Transform buildingTransform, UniqueBuildingViewModel uniqueBuildingModel)
    {
        if (FieldManager.CurrentSelectedTile == null)
        {
            buildingTransform.position = uniqueBuildingModel.Position;
            SetButtonVisibility(uniqueBuildingModel.Id);
        }
        else
        {
            buildingTransform.position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
            uniqueBuildingModel.Position = buildingTransform.position;
        }
        
    }

    private void SetButtonVisibility(int buttonId)
    {
        FieldManager.UniqueBuildingButtons[buttonId].IsAdded = true;
    }

    /// Subscribes to collection modifications.  Add & Remove methods are invoked for each modification.
    public override void UniqueBuildingsAdded(BuildingViewModel item) {
        base.UniqueBuildingsAdded(item);
        CreateUniqueBuilding((UniqueBuildingViewModel)item);
    }

    private void CreateUniqueBuilding(UniqueBuildingViewModel uniqueBuilding)
    {
        switch (uniqueBuilding.Type)
        {
            case UniqueBuildingType.ElfTown:
                var elfTown = Instantiate(ElfTown) as GameObject;
                var elfTownModel = elfTown.GetComponent<ElfTownView>().ElfTown;
                FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = true;
                elfTownModel.Type = uniqueBuilding.Type;
                elfTownModel.Id = uniqueBuilding.Id;
                elfTownModel.IsAdded = uniqueBuilding.IsAdded;
                elfTown.transform.parent = UniqueBuildingParent;
                SetUniqueBuildingPosition(elfTown.transform, uniqueBuilding);
                break;
            case UniqueBuildingType.OrcTown:
                var orcTown = Instantiate(OrcTown) as GameObject;
                var orcTownModel = orcTown.GetComponent<OrcTownView>().OrcTown;
                FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = true;
                orcTownModel.Type = uniqueBuilding.Type;
                orcTownModel.Id = uniqueBuilding.Id;
                orcTownModel.IsAdded = uniqueBuilding.IsAdded;
                orcTown.transform.parent = UniqueBuildingParent;
                SetUniqueBuildingPosition(orcTown.transform, uniqueBuilding);
                break;
            case UniqueBuildingType.HumanTown:
                var humanTown = Instantiate(HumanTown) as GameObject;
                var humanTownModel = humanTown.GetComponent<HumanTownView>().HumanTown;
                FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = true;
                humanTownModel.Type = uniqueBuilding.Type;
                humanTownModel.Id = uniqueBuilding.Id;
                humanTownModel.IsAdded = uniqueBuilding.IsAdded;
                humanTown.transform.parent = UniqueBuildingParent;
                SetUniqueBuildingPosition(humanTown.transform, uniqueBuilding);
                break;
            case UniqueBuildingType.GamerTown:
                var gamerTown = Instantiate(GamerTown) as GameObject;
                var gamerTownModel = gamerTown.GetComponent<GamerTownView>().GamerTown;
                FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = true;
                gamerTownModel.Type = uniqueBuilding.Type;
                gamerTownModel.Id = uniqueBuilding.Id;
                gamerTownModel.IsAdded = uniqueBuilding.IsAdded;
                gamerTown.transform.parent = UniqueBuildingParent;
                SetUniqueBuildingPosition(gamerTown.transform, uniqueBuilding);
                break;
            case UniqueBuildingType.MagicForest:
                var magicForest = Instantiate(MagicForest) as GameObject;
                var magicForestModel = magicForest.GetComponent<MagicForestView>().MagicForest;
                FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = true;
                magicForestModel.Type = uniqueBuilding.Type;
                magicForestModel.Id = uniqueBuilding.Id;
                magicForestModel.IsAdded = uniqueBuilding.IsAdded;
                magicForest.transform.parent = UniqueBuildingParent;
                SetUniqueBuildingPosition(magicForest.transform, uniqueBuilding);
                break;
            case UniqueBuildingType.KnightsHall:
                var knightsHall = Instantiate(KnightsHall) as GameObject;
                var knightsHallModel = knightsHall.GetComponent<KnightsHallView>().KnightsHall;
                FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = true;
                knightsHallModel.Type = uniqueBuilding.Type;
                knightsHallModel.Id = uniqueBuilding.Id;
                knightsHallModel.IsAdded = uniqueBuilding.IsAdded;
                knightsHallModel.Position = uniqueBuilding.Position;
                knightsHall.transform.parent = UniqueBuildingParent;
                SetUniqueBuildingPosition(knightsHall.transform, uniqueBuilding);
                break;
            case UniqueBuildingType.ObserverEye:
                var observerEye = Instantiate(ObserverEye) as GameObject;
                var observerEyeModel = observerEye.GetComponent<ObserverEyeView>().ObserverEye;
                FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = true;
                observerEyeModel.Type = uniqueBuilding.Type;
                observerEyeModel.Id = uniqueBuilding.Id;
                observerEyeModel.IsAdded = uniqueBuilding.IsAdded;
                observerEye.transform.parent = UniqueBuildingParent;
                SetUniqueBuildingPosition(observerEye.transform, uniqueBuilding);
                break;
            case UniqueBuildingType.PotterGenerator:
                var potterGenerator = Instantiate(PotterGenerator) as GameObject;
                var potterGeneratorModel = potterGenerator.GetComponent<PotterGeneratorView>().PotterGenerator;
                FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = true;
                potterGeneratorModel.Type = uniqueBuilding.Type;
                potterGeneratorModel.Id = uniqueBuilding.Id;
                potterGeneratorModel.IsAdded = uniqueBuilding.IsAdded;
                potterGenerator.transform.parent = UniqueBuildingParent;
                SetUniqueBuildingPosition(potterGenerator.transform, uniqueBuilding);
                break;
            case UniqueBuildingType.MonsterSlapper:
                var monsterSlapper = Instantiate(MonsterSlapper) as GameObject;
                var monsterSlapperModel = monsterSlapper.GetComponent<MonsterSlapperView>().MonsterSlapper;
                FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = true;
                monsterSlapperModel.Type = uniqueBuilding.Type;
                monsterSlapperModel.Id = uniqueBuilding.Id;
                monsterSlapperModel.IsAdded = uniqueBuilding.IsAdded;
                monsterSlapper.transform.parent = UniqueBuildingParent;
                SetUniqueBuildingPosition(monsterSlapper.transform, uniqueBuilding);
                break;
            case UniqueBuildingType.GreenBank:
                var greenBank = Instantiate(GreenBank) as GameObject;
                var greenBankModel = greenBank.GetComponent<GreenBankView>().GreenBank;
                FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = true;
                greenBankModel.Type = uniqueBuilding.Type;
                greenBankModel.Id = uniqueBuilding.Id;
                greenBankModel.IsAdded = uniqueBuilding.IsAdded;
                greenBank.transform.parent = UniqueBuildingParent;
                SetUniqueBuildingPosition(greenBank.transform, uniqueBuilding);
                break;
            case UniqueBuildingType.SwordGenerator:
                var swordGenerator = Instantiate(SwordGenerator) as GameObject;
                var swordGeneratorModel = swordGenerator.GetComponent<SwordGeneratorView>().SwordGenerator;
                FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = true;
                swordGeneratorModel.Type = uniqueBuilding.Type;
                swordGeneratorModel.Id = uniqueBuilding.Id;
                swordGeneratorModel.IsAdded = uniqueBuilding.IsAdded;
                swordGenerator.transform.parent = UniqueBuildingParent;
                SetUniqueBuildingPosition(swordGenerator.transform, uniqueBuilding);
                break;
            case UniqueBuildingType.DragoonSlayer:
                var dragoonSlayer = Instantiate(DragoonSlayer) as GameObject;
                var dragoonSlayerModel = dragoonSlayer.GetComponent<DragoonSlayerView>().DragoonSlayer;
                FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = true;
                dragoonSlayerModel.Type = uniqueBuilding.Type;
                dragoonSlayerModel.Id = uniqueBuilding.Id;
                dragoonSlayerModel.IsAdded = uniqueBuilding.IsAdded;
                dragoonSlayer.transform.parent = UniqueBuildingParent;
                SetUniqueBuildingPosition(dragoonSlayer.transform, uniqueBuilding);
                break;
            case UniqueBuildingType.WiseManGenerator:
                var wiseManGenerator = Instantiate(WiseManGenerator) as GameObject;
                var wiseManGeneratorModel = wiseManGenerator.GetComponent<WiseManGeneratorView>().WiseManGenerator;
                FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = true;
                wiseManGeneratorModel.Type = uniqueBuilding.Type;
                wiseManGeneratorModel.Id = uniqueBuilding.Id;
                wiseManGeneratorModel.IsAdded = uniqueBuilding.IsAdded;
                wiseManGenerator.transform.parent = UniqueBuildingParent;
                SetUniqueBuildingPosition(wiseManGenerator.transform, uniqueBuilding);
                break;
            case UniqueBuildingType.EarthShaker:
                var earthShaker = Instantiate(EarthShaker) as GameObject;
                var earthShakerModel = earthShaker.GetComponent<EarthShakerView>().EarthShaker;
                FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = true;
                earthShakerModel.Type = uniqueBuilding.Type;
                earthShakerModel.Id = uniqueBuilding.Id;
                earthShakerModel.IsAdded = uniqueBuilding.IsAdded;
                earthShaker.transform.parent = UniqueBuildingParent;
                SetUniqueBuildingPosition(earthShaker.transform, uniqueBuilding);
                break;
            case UniqueBuildingType.MagicShield:
                var magicShield = Instantiate(MagicShield) as GameObject;
                var magicShieldModel = magicShield.GetComponent<MagicShieldView>().MagicShield;
                FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = true;
                magicShieldModel.Type = uniqueBuilding.Type;
                magicShieldModel.Id = uniqueBuilding.Id;
                magicShieldModel.IsAdded = uniqueBuilding.IsAdded;
                magicShield.transform.parent = UniqueBuildingParent;
                SetUniqueBuildingPosition(magicShield.transform, uniqueBuilding);
                break;
            default:
                break;
        }
    }

    /// Subscribes to collection modifications.  Add & Remove methods are invoked for each modification.
    public override void UniqueBuildingsRemoved(BuildingViewModel item) {
        base.UniqueBuildingsRemoved(item);
    }

    public override void Update()
    {
        base.Update();
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(ray.origin,ray.direction,Color.red);
            if (Physics.Raycast(ray,out hit,float.MaxValue,FieldLayerMask))
            {
                SetSelectedTile(hit.transform.GetComponent<TileView>().Tile);
            }

            if (Physics.Raycast(ray, out hit, float.MaxValue, UniqueBuildingLayerMask))
            {
                SetSelectedUniqueBuilding(hit.transform.GetComponent<UniqueBuildingView>().UniqueBuilding);
            }

            if (Physics.Raycast(ray, out hit, float.MaxValue, CommonBuildingLayerMask))
            {
                SetSelectedCommonBuilding(hit.transform.GetComponent<CommonBuildingView>().CommonBuilding);
            }
        }
    }

    private void SetSelectedTile(TileViewModel tile)
    {
        if (FieldManager.CurrentSelectedTile == null)
        {
            FieldManager.CurrentSelectedTile = tile;
            FieldManager.OldSelectedTile = tile;
        }
        else
        {
            FieldManager.OldSelectedTile = FieldManager.CurrentSelectedTile;
            FieldManager.CurrentSelectedTile = tile;
        }

        FieldManager.OldSelectedTile.IsSelected = false;
        FieldManager.CurrentSelectedTile.IsSelected = true;

    }

    private void SetSelectedUniqueBuilding(UniqueBuildingViewModel uniqueBuilding)
    {
        if (FieldManager.CurrentUniqueBuilding == null)
        {
            FieldManager.CurrentUniqueBuilding = uniqueBuilding;
            FieldManager.OldUniqueBuilding = uniqueBuilding;
        }
        else
        {
            FieldManager.OldUniqueBuilding = FieldManager.CurrentUniqueBuilding;
            FieldManager.CurrentUniqueBuilding = uniqueBuilding;
        }


        FieldManager.OldUniqueBuilding.IsSelected = false;
        FieldManager.CurrentUniqueBuilding.IsSelected = true;
    }

    private void SetSelectedCommonBuilding(CommonBuildingViewModel commonBuilding)
    {
        if (FieldManager.CurrentCommonBuilding == null)
        {
            FieldManager.CurrentCommonBuilding = commonBuilding;
            FieldManager.OldCommonBuilding = commonBuilding;
        }
        else
        {
            FieldManager.OldCommonBuilding = FieldManager.CurrentCommonBuilding;
            FieldManager.CurrentCommonBuilding = commonBuilding;
        }

      
        FieldManager.OldCommonBuilding.IsSelected = false;
        FieldManager.CurrentCommonBuilding.IsSelected = true;
    }


}
