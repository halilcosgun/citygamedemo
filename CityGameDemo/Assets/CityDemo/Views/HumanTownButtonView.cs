using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;


public partial class HumanTownButtonView {

    /// Subscribes to the property and is notified anytime the value changes.
    public override void IsAddedChanged(Boolean isAdded)
    {
        base.IsAddedChanged(isAdded);
        if (isAdded)
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
        }

    }

}
