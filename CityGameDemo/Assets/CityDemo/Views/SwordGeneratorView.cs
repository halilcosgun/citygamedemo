using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;


public partial class SwordGeneratorView {

    /// Subscribes to the property and is notified anytime the value changes.
    public override void IsDeletedChanged(Boolean isDeleted)
    {
        base.IsDeletedChanged(isDeleted);
        if (isDeleted)
        {
            ExecuteDelete();
            Destroy(gameObject);
        }
    }

    public void Delete()
    {
        SwordGenerator.IsDeleted = true;
    }

}
