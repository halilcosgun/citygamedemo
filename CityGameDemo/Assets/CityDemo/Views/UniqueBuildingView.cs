using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;


public partial class UniqueBuildingView { 

    public GameObject UniqueBuildingPopUp;

    /// Subscribes to the property and is notified anytime the value changes.
    public override void IsSelectedChanged(Boolean isSelected) {
        base.IsSelectedChanged(isSelected);
        UniqueBuildingPopUp.SetActive(isSelected);

    }

}
