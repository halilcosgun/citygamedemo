using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public partial class UniqueBuildingButtonListView {

    public GameObject ElfTownButtonButton;
    public GameObject OrcTownButton;
    public GameObject HumanTownButton;
    public GameObject GamerTownButton;
    public GameObject MagicForestButton;
    public GameObject KnightsHallButton;
    public GameObject ObserverEyeButton;
    public GameObject PotterGeneratorButton;
    public GameObject MonsterSlapperButton;
    public GameObject GreenBankButton;
    public GameObject SwordGeneratorButton;
    public GameObject DragoonSlayerButton;
    public GameObject WiseManGeneratorButton;
    public GameObject EarthShakerButton;
    public GameObject MagicShieldButton;

    private List<UniqueBuildingViewModel> _uniqueBuildingButtonData;

    /// Subscribes to collection modifications.  Add & Remove methods are invoked for each modification.
    public override void UniqueBuildingButtonsAdded(UniqueBuildingViewModel item) {
        base.UniqueBuildingButtonsAdded(item);
        CreateUniqueBuildingButton(item);
    }

   
    /// Subscribes to collection modifications.  Add & Remove methods are invoked for each modification.
    public override void UniqueBuildingButtonsRemoved(UniqueBuildingViewModel item) {
        base.UniqueBuildingButtonsRemoved(item);
    }

    public override void Awake()
    {
        base.Awake();
        InitButtonData();
        CreateAllButtons();
    }

    private void CreateAllButtons()
    {
        for (int j = 0; j < _uniqueBuildingButtonData.Count; j++)
        {
            FieldManager.UniqueBuildingButtons.Add(_uniqueBuildingButtonData[j]);
        }
    }

    //initiate button data
    private void InitButtonData()
    {  
        _uniqueBuildingButtonData = new List<UniqueBuildingViewModel>();
        //button virtual list created start
        var uniqueBuildingButtonData = new UniqueBuildingButtonData();
        _uniqueBuildingButtonData = uniqueBuildingButtonData.GetButtonData();
    }


    private void CreateUniqueBuildingButton(UniqueBuildingViewModel item)
    {
        switch (item.Type)
        {
            //E�er bu kodu a�arsam sistem istedi�im gibi �al��m�yor. Sadece Elftown'� devre d��� b�rak�nca,
            //akl�mdaki mimariyi uygulayabiliyorum. Sorun �zerine �al���yorum.
            case UniqueBuildingType.ElfTown:
                //var elfTownButton = Instantiate(ElfTownButtonButton) as GameObject;
                //var elfTownModel = elfTownButton.GetComponent<ElfTownButtonView>().ElfTown;
                //FieldManager.UniqueBuildingButtons[item.Id] = elfTownModel;
                //elfTownModel.Type = item.Type;
                //elfTownModel.Id = item.Id;
              
                //AddTriggerToButton(elfTownButton,elfTownModel);
                //elfTownButton.transform.parent = transform;
                break;
            case UniqueBuildingType.OrcTown:
                var orcTownButton = Instantiate(OrcTownButton) as GameObject;
                var orcTownModel = orcTownButton.GetComponent<OrcTownButtonView>().OrcTown;
                FieldManager.UniqueBuildingButtons[item.Id] = orcTownModel;
                orcTownModel.Type = item.Type;
                orcTownModel.Id = item.Id;
               
                AddTriggerToButton(orcTownButton, orcTownModel);
                orcTownButton.transform.parent = transform;
                break;
            case UniqueBuildingType.HumanTown:
                var humanTownButton = Instantiate(HumanTownButton) as GameObject;
                var humanTownModel = humanTownButton.GetComponent<HumanTownButtonView>().HumanTown;
                FieldManager.UniqueBuildingButtons[item.Id] = humanTownModel;
                humanTownModel.Type = item.Type;
                humanTownModel.Id = item.Id;
              
                AddTriggerToButton(humanTownButton, humanTownModel);
                humanTownButton.transform.parent = transform;
                break;
            case UniqueBuildingType.GamerTown:
                var gamerTownButton = Instantiate(GamerTownButton) as GameObject;
                var gamerTownModel = gamerTownButton.GetComponent<GamerTownButtonView>().GamerTown;
                FieldManager.UniqueBuildingButtons[item.Id] = gamerTownModel;
                gamerTownModel.Type = item.Type;
                gamerTownModel.Id = item.Id;
               
                AddTriggerToButton(gamerTownButton, gamerTownModel);
                gamerTownButton.transform.parent = transform;
                break;
            case UniqueBuildingType.MagicForest:
                var magicForestButton = Instantiate(MagicForestButton) as GameObject;
                var magicForestModel = magicForestButton.GetComponent<MagicForestButtonView>().MagicForest;
                FieldManager.UniqueBuildingButtons[item.Id] = magicForestModel;
                magicForestModel.Type = item.Type;
                magicForestModel.Id = item.Id;
               
                AddTriggerToButton(magicForestButton, magicForestModel);
                magicForestButton.transform.parent = transform;
                break;
            case UniqueBuildingType.KnightsHall:
                var knightsHallButton = Instantiate(KnightsHallButton) as GameObject;
                var knightsHallModel = knightsHallButton.GetComponent<KnightsHallButtonView>().KnightsHall;
                FieldManager.UniqueBuildingButtons[item.Id] = knightsHallModel;
                knightsHallModel.Type = item.Type;
                knightsHallModel.Id = item.Id;
               
                AddTriggerToButton(knightsHallButton, knightsHallModel);
                knightsHallButton.transform.parent = transform;
                break;
            case UniqueBuildingType.ObserverEye:
                var observerEyeButton = Instantiate(ObserverEyeButton) as GameObject;
                var observerEyeModel = observerEyeButton.GetComponent<ObserverEyeButtonView>().ObserverEye;
                FieldManager.UniqueBuildingButtons[item.Id] = observerEyeModel;
                observerEyeModel.Type = item.Type;
                observerEyeModel.Id = item.Id;
           
                AddTriggerToButton(observerEyeButton, observerEyeModel);
                observerEyeButton.transform.parent = transform;
                break;
            case UniqueBuildingType.PotterGenerator:
                var potterGeneratorButton = Instantiate(PotterGeneratorButton) as GameObject;
                var potterGeneratorModel = potterGeneratorButton.GetComponent<PotterGeneratorButtonView>().PotterGenerator;
                FieldManager.UniqueBuildingButtons[item.Id] = potterGeneratorModel;
                potterGeneratorModel.Type = item.Type;
                potterGeneratorModel.Id = item.Id;
              
                AddTriggerToButton(potterGeneratorButton, potterGeneratorModel);
                potterGeneratorButton.transform.parent = transform;
                break;
            case UniqueBuildingType.MonsterSlapper:
                var monsterSlapperButton = Instantiate(MonsterSlapperButton) as GameObject;
                var monsterSlapperModel = monsterSlapperButton.GetComponent<MonsterSlapperButtonView>().MonsterSlapper;
                FieldManager.UniqueBuildingButtons[item.Id] = monsterSlapperModel;
                monsterSlapperModel.Type = item.Type;
                monsterSlapperModel.Id = item.Id;
               
                AddTriggerToButton(monsterSlapperButton, monsterSlapperModel);
                monsterSlapperButton.transform.parent = transform;
                break;
            case UniqueBuildingType.GreenBank:
                var greenBankButton = Instantiate(GreenBankButton) as GameObject;
                var greenBankModel = greenBankButton.GetComponent<GreenBankButtonView>().GreenBank;
                FieldManager.UniqueBuildingButtons[item.Id] = greenBankModel;
                greenBankModel.Type = item.Type;
                greenBankModel.Id = item.Id;
           
                AddTriggerToButton(greenBankButton, greenBankModel);
                greenBankButton.transform.parent = transform;
                break;
            case UniqueBuildingType.SwordGenerator:
                var swordGeneratorButton = Instantiate(SwordGeneratorButton) as GameObject;
                var swordGeneratorModel = swordGeneratorButton.GetComponent<SwordGeneratorButtonView>().SwordGenerator;
                FieldManager.UniqueBuildingButtons[item.Id] = swordGeneratorModel;
                swordGeneratorModel.Type = item.Type;
                swordGeneratorModel.Id = item.Id;
             
                AddTriggerToButton(swordGeneratorButton, swordGeneratorModel);
                swordGeneratorButton.transform.parent = transform;
                break;
            case UniqueBuildingType.DragoonSlayer:
                var dragoonSlayerButton = Instantiate(DragoonSlayerButton) as GameObject;
                var dragoonSlayerModel = dragoonSlayerButton.GetComponent<DragoonSlayerButtonView>().DragoonSlayer;
                FieldManager.UniqueBuildingButtons[item.Id] = dragoonSlayerModel;
                dragoonSlayerModel.Type = item.Type;
                dragoonSlayerModel.Id = item.Id;
             
                AddTriggerToButton(dragoonSlayerButton, dragoonSlayerModel);
                dragoonSlayerButton.transform.parent = transform;
                break;
            case UniqueBuildingType.WiseManGenerator:
                var wiseManGeneratorButton = Instantiate(WiseManGeneratorButton) as GameObject;
                var wiseManGeneratorModel = wiseManGeneratorButton.GetComponent<WiseManGeneratorButtonView>().WiseManGenerator;
                FieldManager.UniqueBuildingButtons[item.Id] = wiseManGeneratorModel;
                wiseManGeneratorModel.Type = item.Type;
                wiseManGeneratorModel.Id = item.Id;
             
                AddTriggerToButton(wiseManGeneratorButton, wiseManGeneratorModel);
                wiseManGeneratorButton.transform.parent = transform;
                break;
            case UniqueBuildingType.EarthShaker:
                var earthShakerButton = Instantiate(EarthShakerButton) as GameObject;
                var earthShakerModel = earthShakerButton.GetComponent<EarthShakerButtonView>().EarthShaker;
                FieldManager.UniqueBuildingButtons[item.Id] = earthShakerModel;
                earthShakerModel.Type = item.Type;
                earthShakerModel.Id = item.Id;
              
                AddTriggerToButton(earthShakerButton, earthShakerModel);
                earthShakerButton.transform.parent = transform;
                break;
            case UniqueBuildingType.MagicShield:
                var magicShieldButton = Instantiate(MagicShieldButton) as GameObject;
                var magicShieldModel = magicShieldButton.GetComponent<MagicShieldButtonView>().MagicShield;
                FieldManager.UniqueBuildingButtons[item.Id] = magicShieldModel;
                magicShieldModel.Type = item.Type;
                magicShieldModel.Id = item.Id;
             
                AddTriggerToButton(magicShieldButton, magicShieldModel);
                magicShieldButton.transform.parent = transform;
                break;
            default:
                break;
        }
    }

    private void AddTriggerToButton(GameObject go , UniqueBuildingViewModel uniqueBuildingViewModel)
    {
        EventTrigger trigger = go.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;

        entry.callback.AddListener((eventData) =>
        {
            var currentTileId = FieldManager.CurrentSelectedTile.Id;

            if ((currentTileId % FieldManager.TileCountLine < FieldManager.TileCountLine - 1) && currentTileId < 
                (FieldManager.TileCountLine * FieldManager.TileCountColumn - 1))
            {
                if (FieldManager.Tiles[currentTileId].IsEmpty &&
              FieldManager.Tiles[currentTileId + 1].IsEmpty &&
              FieldManager.Tiles[currentTileId + FieldManager.TileCountLine].IsEmpty &&
              FieldManager.Tiles[currentTileId + FieldManager.TileCountLine + 1].IsEmpty)
                {
                    FieldManager.UniqueBuildingButtons[uniqueBuildingViewModel.Id].IsAdded = true;
                }
            }           
            ExecuteInstantiateUniqueBuilding(uniqueBuildingViewModel);
            
            
        });

        trigger.triggers.Add(entry);
    }



    private class UniqueBuildingButtonData
    {
        public List<UniqueBuildingViewModel> UniqueBuildingButtonsData;

        public UniqueBuildingButtonData()
        {

            UniqueBuildingButtonsData = new List<UniqueBuildingViewModel>();

            var elfTownModel = CreateUniqueBuildingData(UniqueBuildingType.ElfTown);
            var orcTownModel = CreateUniqueBuildingData(UniqueBuildingType.OrcTown);
            var humanTownModel = CreateUniqueBuildingData(UniqueBuildingType.HumanTown);
            var gamerTownModel = CreateUniqueBuildingData(UniqueBuildingType.GamerTown);
            var magicForestModel = CreateUniqueBuildingData(UniqueBuildingType.MagicForest);
            var knightsHallModel = CreateUniqueBuildingData(UniqueBuildingType.KnightsHall);
            var observerEyeModel = CreateUniqueBuildingData(UniqueBuildingType.ObserverEye);
            var potterGeneratorModel = CreateUniqueBuildingData(UniqueBuildingType.PotterGenerator);
            var monsterSlapperModel = CreateUniqueBuildingData(UniqueBuildingType.MonsterSlapper);
            var greenBankModel = CreateUniqueBuildingData(UniqueBuildingType.GreenBank);
            var swordGeneratorModel = CreateUniqueBuildingData(UniqueBuildingType.SwordGenerator);
            var dragoonSlayerModel = CreateUniqueBuildingData(UniqueBuildingType.DragoonSlayer);
            var wiseManGeneratorModel = CreateUniqueBuildingData(UniqueBuildingType.WiseManGenerator);
            var earthShakerModel = CreateUniqueBuildingData(UniqueBuildingType.EarthShaker);
            var magicShieldModel = CreateUniqueBuildingData(UniqueBuildingType.MagicShield);

            UniqueBuildingButtonsData.Add(elfTownModel);//0
            UniqueBuildingButtonsData.Add(orcTownModel);//1
            UniqueBuildingButtonsData.Add(humanTownModel);//2
            UniqueBuildingButtonsData.Add(gamerTownModel);//3
            UniqueBuildingButtonsData.Add(magicForestModel);//4
            UniqueBuildingButtonsData.Add(knightsHallModel);//5
            UniqueBuildingButtonsData.Add(observerEyeModel);//6
            UniqueBuildingButtonsData.Add(potterGeneratorModel);//7
            UniqueBuildingButtonsData.Add(monsterSlapperModel);//8
            UniqueBuildingButtonsData.Add(greenBankModel);//9
            UniqueBuildingButtonsData.Add(swordGeneratorModel);//10
            UniqueBuildingButtonsData.Add(dragoonSlayerModel);//11
            UniqueBuildingButtonsData.Add(wiseManGeneratorModel);//12
            UniqueBuildingButtonsData.Add(earthShakerModel);//13
            UniqueBuildingButtonsData.Add(magicShieldModel);//14
            SetButtonIds();
        }

        private UniqueBuildingViewModel CreateUniqueBuildingData(UniqueBuildingType type)
        {
            var uniqueBuildingModel = new UniqueBuildingViewModel();
            uniqueBuildingModel.Type = type;
            return uniqueBuildingModel;
        }

        private void SetButtonIds()
        {
            for (int i = 0; i < UniqueBuildingButtonsData.Count; i++)
            {
                UniqueBuildingButtonsData[i].Id = i;
            }
        }

        public List<UniqueBuildingViewModel> GetButtonData()
        {
            return UniqueBuildingButtonsData;
        }
    }
}
