using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using UniRx;
using UnityEngine;
using System.Xml.Linq;
using System.IO;

public class FieldManagerController : FieldManagerControllerBase
{
    private int _tileCountColumn;
    private int _tileCountLine;
    private Sprite _tileSprite;
    private bool[,] _tileStatesArray;

    public override void InitializeFieldManager(FieldManagerViewModel fieldManager)
    {
        FieldManager.TileCountLine = 20;
        FieldManager.TileCountColumn = 20;
        _tileCountLine = FieldManager.TileCountLine;
        _tileCountColumn = FieldManager.TileCountColumn;
        _tileSprite = Resources.Load<Sprite>("Art/GreenTile");
        _tileStatesArray = new bool[_tileCountLine, _tileCountColumn];
        SetupField(fieldManager);
    }

    public override void SetupField(FieldManagerViewModel fieldManager)
    {
        base.SetupField(fieldManager);
        GenerateTiles(_tileCountLine, _tileCountColumn);
    }

    /// <summary>
    /// Generate the tile objects on the field.
    /// </summary>
    /// <param name="line">How many tiles in a line </param>
    /// <param name="column">How many columns in a line </param>
    private void GenerateTiles(int line , int column)
    {
        var tileContainer = new GameObject("TileContainer");
        var startPosition = CalculateStartPosition(line, column);
        var temp = startPosition;
        var id = 0;

        for (var i = 0; i < line; i++)
        {
            for (var j = 0; j < column; j++)
            {
                var tile = new GameObject("Tile" + i + "x" + j);
                tile.transform.parent = tileContainer.transform;
                tile.AddComponent<BoxCollider>();
                tile.AddComponent<SpriteRenderer>().sprite = _tileSprite;
                tile.layer = LayerMask.NameToLayer("Ground");
                tile.transform.position = startPosition;
                tile.AddComponent<TileView>().Tile.Id = id;
                id++;
                startPosition += new Vector2(1,0);
                if (j==column-1)
                {
                    startPosition = new Vector2(temp.x,startPosition.y - 1);
                }
                
                FieldManager.Tiles.Add(tile.GetComponent<TileView>().Tile);
            }
        }
    }

    /// <summary>
    /// Calculate the start position of first tile
    /// </summary>
    /// <param name="objectCountInALine">How many tiles in a line</param>
    /// <param name="objectCountInAColumn">How many colums in a line</param>
    /// <returns></returns>
    private Vector2 CalculateStartPosition(int objectCountInALine, int objectCountInAColumn)
    {
        var xPos = 0f;
        var yPos = 0f;
        xPos = CheckIsPair(objectCountInALine) ? objectCountInALine/2 : objectCountInALine/2 - 0.5f;
        yPos = CheckIsPair(objectCountInAColumn) ? objectCountInAColumn/2 : objectCountInAColumn/2 - 0.5f;
        return new Vector2(-xPos,yPos);
    }

    /// <summary>
    /// Check is the number pair or not
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    private bool CheckIsPair(int number)
    {
        var divided = number/2;
        return divided * 2 == number;
       
    }

    public override void InstantiateCommonBuilding(FieldManagerViewModel fieldManager, CommonBuildingViewModel item)
    {
        base.InstantiateCommonBuilding(fieldManager, item);
        if (FieldManager.CurrentSelectedTile.IsEmpty != false)
        {
            switch (item.Type)
            {
                case CommonBuildingType.Barrack:
                    BarrackViewModel barrack = new BarrackViewModel();
                    barrack.IsSelected = false;
                    barrack.Position = fieldManager.CurrentSelectedTile.Index;
                    barrack.Type = CommonBuildingType.Barrack;
                    fieldManager.CommonBuildings.Add(barrack);
                    break;
                case CommonBuildingType.Tower:
                    TowerViewModel tower = new TowerViewModel();
                    tower.IsSelected = false;
                    tower.Position = fieldManager.CurrentSelectedTile.Index;
                    tower.Type = CommonBuildingType.Tower;
                    fieldManager.CommonBuildings.Add(tower);
                    break;
                case CommonBuildingType.Home:
                    HomeViewModel home = new HomeViewModel();
                    home.IsSelected = false;
                    home.Position = fieldManager.CurrentSelectedTile.Index;
                    home.Type = CommonBuildingType.Home;
                    fieldManager.CommonBuildings.Add(home);
                    break;
                case CommonBuildingType.Farm:
                    FarmViewModel farm = new FarmViewModel();
                    farm.IsSelected = false;
                    farm.Position = fieldManager.CurrentSelectedTile.Index;
                    farm.Type = CommonBuildingType.Farm;
                    fieldManager.CommonBuildings.Add(farm);
                    break;
                case CommonBuildingType.Wall:
                    WallViewModel wall = new WallViewModel();
                    wall.IsSelected = false;
                    wall.Position = fieldManager.CurrentSelectedTile.Index;
                    wall.Type = CommonBuildingType.Wall;
                    fieldManager.CommonBuildings.Add(wall);
                    break;
                default:
                    break;
            }
        }
       
    }

    public override void InstantiateUniqueBuilding(FieldManagerViewModel fieldManager, UniqueBuildingViewModel uniqueBuildingModel)
    {
        base.InstantiateUniqueBuilding(fieldManager, uniqueBuildingModel);
        if (CheckTilesForUniqueBuildings(FieldManager.CurrentSelectedTile.Id))
        {
            switch (uniqueBuildingModel.Type)
            {
                case UniqueBuildingType.ElfTown:
                    ElfTownViewModel elfTown = new ElfTownViewModel();
                    elfTown.Type = uniqueBuildingModel.Type;
                    elfTown.Id = uniqueBuildingModel.Id;
                    elfTown.IsAdded = uniqueBuildingModel.IsAdded;
                    elfTown.Position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
                    fieldManager.UniqueBuildings.Add(elfTown);
                    break;
                case UniqueBuildingType.OrcTown:
                    OrcTownViewModel orcTown = new OrcTownViewModel();
                    orcTown.Type = uniqueBuildingModel.Type;
                    orcTown.Id = uniqueBuildingModel.Id;
                    orcTown.IsAdded = uniqueBuildingModel.IsAdded;
                    orcTown.Position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
                    fieldManager.UniqueBuildings.Add(orcTown);
                    break;
                case UniqueBuildingType.HumanTown:
                    HumanTownViewModel humanTown = new HumanTownViewModel();
                    humanTown.Type = uniqueBuildingModel.Type;
                    humanTown.Id = uniqueBuildingModel.Id;
                    humanTown.IsAdded = uniqueBuildingModel.IsAdded;
                    humanTown.Position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
                    fieldManager.UniqueBuildings.Add(humanTown);
                    break;
                case UniqueBuildingType.GamerTown:
                    GamerTownViewModel gamerTown = new GamerTownViewModel();
                    gamerTown.Type = uniqueBuildingModel.Type;
                    gamerTown.Id = uniqueBuildingModel.Id;
                    gamerTown.IsAdded = uniqueBuildingModel.IsAdded;
                    gamerTown.Position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
                    fieldManager.UniqueBuildings.Add(gamerTown);
                    break;
                case UniqueBuildingType.MagicForest:
                    MagicForestViewModel magicForest = new MagicForestViewModel();
                    magicForest.Type = uniqueBuildingModel.Type;
                    magicForest.Id = uniqueBuildingModel.Id;
                    magicForest.IsAdded = uniqueBuildingModel.IsAdded;
                    magicForest.Position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
                    fieldManager.UniqueBuildings.Add(magicForest);
                    break;
                case UniqueBuildingType.KnightsHall:
                    KnightsHallViewModel knightsHall = new KnightsHallViewModel();
                    knightsHall.Type = uniqueBuildingModel.Type;
                    knightsHall.Id = uniqueBuildingModel.Id;
                    knightsHall.IsAdded = uniqueBuildingModel.IsAdded;
                    knightsHall.Position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
                    fieldManager.UniqueBuildings.Add(knightsHall);
                    break;
                case UniqueBuildingType.ObserverEye:
                    ObserverEyeViewModel observerEye = new ObserverEyeViewModel();
                    observerEye.Type = uniqueBuildingModel.Type;
                    observerEye.Id = uniqueBuildingModel.Id;
                    observerEye.IsAdded = uniqueBuildingModel.IsAdded;
                    observerEye.Position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
                    fieldManager.UniqueBuildings.Add(observerEye);
                    break;
                case UniqueBuildingType.PotterGenerator:
                    PotterGeneratorViewModel potterGenerator = new PotterGeneratorViewModel();
                    potterGenerator.Type = uniqueBuildingModel.Type;
                    potterGenerator.Id = uniqueBuildingModel.Id;
                    potterGenerator.IsAdded = uniqueBuildingModel.IsAdded;
                    potterGenerator.Position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
                    fieldManager.UniqueBuildings.Add(potterGenerator);
                    break;
                case UniqueBuildingType.MonsterSlapper:
                    MonsterSlapperViewModel monsterSlapper = new MonsterSlapperViewModel();
                    monsterSlapper.Type = uniqueBuildingModel.Type;
                    monsterSlapper.Id = uniqueBuildingModel.Id;
                    monsterSlapper.IsAdded = uniqueBuildingModel.IsAdded;
                    monsterSlapper.Position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
                    fieldManager.UniqueBuildings.Add(monsterSlapper);
                    break;
                case UniqueBuildingType.GreenBank:
                    GreenBankViewModel greenBank = new GreenBankViewModel();
                    greenBank.Type = uniqueBuildingModel.Type;
                    greenBank.Id = uniqueBuildingModel.Id;
                    greenBank.IsAdded = uniqueBuildingModel.IsAdded;
                    greenBank.Position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
                    fieldManager.UniqueBuildings.Add(greenBank);
                    break;
                case UniqueBuildingType.SwordGenerator:
                    SwordGeneratorViewModel swordGenerator = new SwordGeneratorViewModel();
                    swordGenerator.Type = uniqueBuildingModel.Type;
                    swordGenerator.Id = uniqueBuildingModel.Id;
                    swordGenerator.IsAdded = uniqueBuildingModel.IsAdded;
                    swordGenerator.Position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
                    fieldManager.UniqueBuildings.Add(swordGenerator);
                    break;
                case UniqueBuildingType.DragoonSlayer:
                    DragoonSlayerViewModel dragoonSlayer = new DragoonSlayerViewModel();
                    dragoonSlayer.Type = uniqueBuildingModel.Type;
                    dragoonSlayer.Id = uniqueBuildingModel.Id;
                    dragoonSlayer.IsAdded = uniqueBuildingModel.IsAdded;
                    dragoonSlayer.Position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
                    fieldManager.UniqueBuildings.Add(dragoonSlayer);
                    break;
                case UniqueBuildingType.WiseManGenerator:
                    WiseManGeneratorViewModel wiseManGenerator = new WiseManGeneratorViewModel();
                    wiseManGenerator.Type = uniqueBuildingModel.Type;
                    wiseManGenerator.Id = uniqueBuildingModel.Id;
                    wiseManGenerator.IsAdded = uniqueBuildingModel.IsAdded;
                    wiseManGenerator.Position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
                    fieldManager.UniqueBuildings.Add(wiseManGenerator);
                    break;
                case UniqueBuildingType.EarthShaker:
                    EarthShakerViewModel earthShaker = new EarthShakerViewModel();
                    earthShaker.Type = uniqueBuildingModel.Type;
                    earthShaker.Id = uniqueBuildingModel.Id;
                    earthShaker.IsAdded = uniqueBuildingModel.IsAdded;
                    earthShaker.Position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
                    fieldManager.UniqueBuildings.Add(earthShaker);
                    break;
                case UniqueBuildingType.MagicShield:
                    MagicShieldViewModel magicShield = new MagicShieldViewModel();
                    magicShield.Type = uniqueBuildingModel.Type;
                    magicShield.Id = uniqueBuildingModel.Id;
                    magicShield.IsAdded = uniqueBuildingModel.IsAdded;
                    magicShield.Position = FieldManager.CurrentSelectedTile.Index + new Vector2(.5f, -.5f);
                    fieldManager.UniqueBuildings.Add(magicShield);
                    break;
                default:
                    break;
        }

        ChangeTileCases();
        }

    }

    //set false to tiles after placing unique building
    private void ChangeTileCases()
    {
        var currentTileId = FieldManager.CurrentSelectedTile.Id;
        FieldManager.Tiles[currentTileId].IsEmpty = false;
        FieldManager.Tiles[currentTileId + 1].IsEmpty = false;
        FieldManager.Tiles[currentTileId + _tileCountLine].IsEmpty = false;
        FieldManager.Tiles[currentTileId + _tileCountLine + 1].IsEmpty = false;
        
    }

    public bool CheckTilesForUniqueBuildings(int tileId)
    {
        var tileAvailability = false;
        if ((tileId % _tileCountLine < _tileCountLine - 1) && tileId < ((_tileCountLine - 1) * _tileCountColumn))
        {
            if (FieldManager.Tiles[tileId].IsEmpty && 
                FieldManager.Tiles[tileId + 1].IsEmpty && 
                FieldManager.Tiles[tileId + _tileCountLine].IsEmpty && 
                FieldManager.Tiles[tileId + _tileCountLine + 1].IsEmpty)
            {
                tileAvailability = true;
            }
        }        

        return tileAvailability;
    }

    public override void Save(FieldManagerViewModel fieldManager)
    {
        base.Save(fieldManager);
        XElement data = new XElement("Datas");
        XElement tiles = new XElement("Tiles");
        XElement commonBuildings = new XElement("CommonBuildings");
        XElement uniqueBuildings = new XElement("UniqueBuildings");

        for (int i = 0; i < fieldManager.Tiles.Count; i++)
        {
            tiles.Add(TileToXml(fieldManager.Tiles[i]));
        }

        for (int i = 0; i < fieldManager.CommonBuildings.Count; i++)
        {
            commonBuildings.Add(CommonBuildingToXml((CommonBuildingViewModel)fieldManager.CommonBuildings[i]));
        }

        for (int i = 0; i < fieldManager.UniqueBuildings.Count; i++)
        {
            uniqueBuildings.Add(UniqueBuildingToXml((UniqueBuildingViewModel)fieldManager.UniqueBuildings[i]));
        }

        data.Add(tiles);
        data.Add(commonBuildings);
        data.Add(uniqueBuildings);

        data.Save("GameData.xml",SaveOptions.None);
    }

    public override void Load(FieldManagerViewModel fieldManager)
    {
        base.Load(fieldManager);

        if (File.Exists("GameData.xml"))
        {
            XElement data = XElement.Load("GameData.xml");

            //load tile data
            var tileNumber = 0;
            foreach (var item in data.Elements("Tiles").Elements())
            {

                var tileInfo = BuildTileFromXml(item);
                FieldManager.Tiles[tileNumber].IsEmpty = tileInfo.IsEmpty;
                tileNumber++;
            }

            //load unique building data
            foreach (var item in data.Elements("UniqueBuildings").Elements())
            {
                var buildingModel = BuildUniqueBuildingFromXML(item);
                FieldManager.UniqueBuildings.Add(buildingModel);
            }

            //load common building data
            foreach (var item in data.Elements("CommonBuildings").Elements())
            {
                var commonBuilding = CommonBuildingFromXML(item);
                FieldManager.CommonBuildings.Add(commonBuilding);
            }
        }     

    }

    private CommonBuildingViewModel CommonBuildingFromXML(XElement item)
    {
        var commonBuilding = new CommonBuildingViewModel();
        commonBuilding.IsSelected = Convert.ToBoolean(item.Element("isSelected").Value);

        commonBuilding.Position = new Vector2(
               (float)item.Element("position").Attribute("x"),
               (float)item.Element("position").Attribute("y")
           );

        var type = Convert.ToString(item.Element("type").Value);
        commonBuilding.Type = SetCommonBuildingType(type);

        return commonBuilding;
    }

    

    private TileViewModel BuildTileFromXml(XElement item)
    {
        var tileModel = new TileViewModel();
        tileModel.Id = Convert.ToInt32(item.Element("id").Value);
        tileModel.IsEmpty = Convert.ToBoolean(item.Element("isEmpty").Value);
        return tileModel;
    }

    public UniqueBuildingViewModel BuildUniqueBuildingFromXML(XElement item)
    {
        var uniqueBuildingModel = new UniqueBuildingViewModel();

        uniqueBuildingModel.Id = Convert.ToInt32(item.Element("id").Value);
        uniqueBuildingModel.IsSelected = Convert.ToBoolean(item.Element("isSelected").Value);
        uniqueBuildingModel.IsAdded = true;

        var type = Convert.ToString(item.Element("type").Value);
        uniqueBuildingModel.Type = SetUniqueBuildingType(type);

        uniqueBuildingModel.Position = new Vector2(
                (float)item.Element("position").Attribute("x"),
                (float)item.Element("position").Attribute("y")
            );

        return uniqueBuildingModel;
    }

    private CommonBuildingType SetCommonBuildingType(string type)
    {
        var buildingType = new CommonBuildingType();
        switch (type)
        {
            case "Barrack":
                buildingType = CommonBuildingType.Barrack;
                break;
            case "Tower":
                buildingType = CommonBuildingType.Tower;
                break;
            case "Home":
                buildingType = CommonBuildingType.Home;
                break;
            case "Farm":
                buildingType = CommonBuildingType.Farm;
                break;
            case "Wall":
                buildingType = CommonBuildingType.Wall;
                break;
            default:
                break;
        }

        return buildingType;
    }

    private UniqueBuildingType SetUniqueBuildingType(string type)
    {
        var buildingType = new UniqueBuildingType();
        switch (type)
        {
            case "ElfTown":
                buildingType = UniqueBuildingType.ElfTown;
                break;
            case "OrcTown":
                buildingType = UniqueBuildingType.OrcTown;
                break;
            case "HumanTown":
                buildingType = UniqueBuildingType.HumanTown;
                break;
            case "GamerTown":
                buildingType = UniqueBuildingType.GamerTown;
                break;
            case "MagicForest":
                buildingType = UniqueBuildingType.MagicForest;
                break;
            case "KnightsHall":
                buildingType = UniqueBuildingType.KnightsHall;
                break;
            case "ObserverEye":
                buildingType = UniqueBuildingType.ObserverEye;
                break;
            case "PotterGenerator":
                buildingType = UniqueBuildingType.PotterGenerator;
                break;
            case "MonsterSlapper":
                buildingType = UniqueBuildingType.MonsterSlapper;
                break;
            case "GreenBank":
                buildingType = UniqueBuildingType.GreenBank;
                break;
            case "SwordGenerator":
                buildingType = UniqueBuildingType.SwordGenerator;
                break;
            case "DragoonSlayer":
                buildingType = UniqueBuildingType.DragoonSlayer;
                break;
            case "WiseManGenerator":
                buildingType = UniqueBuildingType.WiseManGenerator;
                break;
            case "EarthShaker":
                buildingType = UniqueBuildingType.EarthShaker;
                break;
            case "MagicShield":
                buildingType = UniqueBuildingType.MagicShield;
                break;
            default:
                break;
        }

        return buildingType;
    }

    //setup the tile xml element
    private XElement TileToXml(TileViewModel tileModel)
    {
        var tile = new XElement("Tile",
            new XElement("id",tileModel.Id),
            new XElement("index",tileModel.Index),
            new XElement("isEmpty", tileModel.IsEmpty),
            new XElement("isSelected", tileModel.IsSelected)            
            );
        return tile;
    }

    //setuo the common building xml element
    private XElement CommonBuildingToXml(CommonBuildingViewModel commonBuildingModel)
    {
        var commonBuilding = new XElement("CommonBuilding",

            new XElement("isSelected", commonBuildingModel.IsSelected),
            new XElement("type", commonBuildingModel.Type),
            
            new XElement("position", 
                            new XAttribute("x",commonBuildingModel.Position.x),
                            new XAttribute("y", commonBuildingModel.Position.y)
                        )
            );

        return commonBuilding;
    }

    //setup the unique building xml element
    private XElement UniqueBuildingToXml(UniqueBuildingViewModel uniqueBuildingModel)
    {
        var uniqueBuilding = new XElement("UniqueBuilding",
            new XElement("id", uniqueBuildingModel.Id),
            new XElement("isAdded", uniqueBuildingModel.IsAdded),
            new XElement("isSelected", uniqueBuildingModel.IsSelected),
            new XElement("type", uniqueBuildingModel.Type),

            new XElement("position",
                            new XAttribute("x", uniqueBuildingModel.Position.x),
                            new XAttribute("y", uniqueBuildingModel.Position.y)
                        )
            );

        return uniqueBuilding;
    }

}
