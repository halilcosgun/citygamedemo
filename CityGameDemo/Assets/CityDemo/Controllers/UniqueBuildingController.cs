using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;


public class UniqueBuildingController : UniqueBuildingControllerBase {
    
    public override void InitializeUniqueBuilding(UniqueBuildingViewModel uniqueBuilding) {
    }

    public override void Delete(UniqueBuildingViewModel uniqueBuilding)
    {
        base.Delete(uniqueBuilding);
        MakeVisibleUniqueBuildingButton(uniqueBuilding);
        FieldManager.UniqueBuildings.Remove(uniqueBuilding);    
        
    }

    private void MakeVisibleUniqueBuildingButton(UniqueBuildingViewModel uniqueBuilding)
    {
        FieldManager.UniqueBuildingButtons[uniqueBuilding.Id].IsAdded = false;
    }
}
